{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from sklearn.datasets import make_circles\n",
    "from sklearn.metrics import accuracy_score\n",
    "from tqdm import tqdm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Fonctions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "def initialisation(n0, n1, n2):\n",
    "    \n",
    "    W1 = np.random.randn(n1, n0)\n",
    "    b1 = np.random.randn(n1, 1)\n",
    "    W2 = np.random.randn(n2, n1)\n",
    "    b2 = np.random.randn(n2, n1)\n",
    "    \n",
    "    parametres = {\n",
    "        'W1': W1,\n",
    "        'b1': b1,\n",
    "        'W2': W2,\n",
    "        'b2': b2\n",
    "    }\n",
    "    \n",
    "    return parametres"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Notre fonction de model que nous allons nommé forward_propagation\n",
    "def forward_propagation(X, parametres):\n",
    "    \n",
    "    W1 = parametres['W1']\n",
    "    b1 = parametres['b1']\n",
    "    W2 = parametres['W2']\n",
    "    b2 = parametres['b2']\n",
    "    \n",
    "    Z1 = W1.dot(X) + b1\n",
    "    A1 = 1 / (1 + np.exp(-Z1))\n",
    "    Z2 = W2.dot(A1) + b2\n",
    "    A2 = 1 / (1 + np.exp(-Z2))\n",
    "    \n",
    "    activations = {\n",
    "        'A1': A1,\n",
    "        'A2': A2\n",
    "    }\n",
    "    \n",
    "    return activations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Notre fonction des gradients que nous allons nommé back_propagation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "def back_propagation(X, y, activations, parametres):\n",
    "    \n",
    "    A1 = activations['A1']\n",
    "    A2 = activations['A2']\n",
    "    W2 = parametres['W2']\n",
    "    \n",
    "    m = y.shape[1]\n",
    "    \n",
    "    dZ2 = A2 - y\n",
    "    dW2 = 1 / m * dZ2.dot(A1.T)\n",
    "    db2 = 1 / m * np.sum(dZ2, axis=1, keepdims=True) # le \"keepdims\" permettra de garder la dimension (à 2 dans notre cas) : effet de Broadcasting\n",
    "    \n",
    "    dZ1 = np.dot(W2.T, dZ2) * A1 * (1 - A1)\n",
    "    dW1 = 1 / m * dZ1.dot(X.T)\n",
    "    db1 = 1 / m * np.sum(dZ1, axis=1, keepdims=True)\n",
    "    \n",
    "    gradients = {\n",
    "        'dW1': dW1,\n",
    "        'db1': db1,\n",
    "        'dW2': dW2,\n",
    "        'db2': db2\n",
    "    }\n",
    "    \n",
    "    return gradients"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "def update(gradients, parametres, learning_rate):\n",
    "    \n",
    "    W1 = parametres['W1']\n",
    "    b1 = parametres['b1']\n",
    "    W2 = parametres['W2']\n",
    "    b2 = parametres['b2']\n",
    "    \n",
    "    dW1 = gradients['dW1']\n",
    "    db1 = gradients['db1']\n",
    "    dW2 = gradients['dW2']\n",
    "    db2 = gradients['db2']\n",
    "    \n",
    "    W1 = W1 - learning_rate * dW1\n",
    "    b1 = b1 - learning_rate * db1\n",
    "    W2 = W2 - learning_rate * dW2\n",
    "    b2 = b2 - learning_rate * db2\n",
    "    \n",
    "    parametres = {\n",
    "        'W1': W1,\n",
    "        'b1': b1,\n",
    "        'W2': W2,\n",
    "        'b2': b2\n",
    "    }\n",
    "    \n",
    "    return parametres"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "def predict(X, parametres):\n",
    "    activations = forward_propagation(X, parametres)\n",
    "    A2 = activations['A2']\n",
    "    return A2 >= 0.5"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "# n1: Nombre de neurones dans notre première couche de neurones\n",
    "def neural_network(W_train, y_train, n1, learning_rate = 0.01, n_iter = 1000):\n",
    "    \n",
    "    # Initialisation de W, b\n",
    "    n0 = X_train.shape[0]\n",
    "    n2 = y_train.shape[0]\n",
    "    parametres = initialisation(n0, n1, n2)\n",
    "    \n",
    "    train_loss = []\n",
    "    train_acc  = []\n",
    "    \n",
    "    for i in tqdm(range(n_iter)):\n",
    "        \n",
    "        activations = forward_propagation(X_train, parametres)\n",
    "        gradients   = back_propagation(X_train, y_train, activations, parametres)\n",
    "        parametres  = update(gradients, parametres, learning_rate)\n",
    "        \n",
    "        if i % 10 == 0:\n",
    "            # Train\n",
    "            train_loss.append(log_loss(y_train, activations['A2']))\n",
    "            y_pred = predict(X_train, parametres)\n",
    "            current_accuracy = accuracy_score(y_train.flatten(), y_pred.flatten())\n",
    "            train_acc.append(current_accuracy)\n",
    "            \n",
    "    plt.figure(figsize=(14, 4))\n",
    "    \n",
    "    plt.subplot(1, 2, 1)\n",
    "    plt.plot(train_loss, label='Train Loss')\n",
    "    plt.legend()\n",
    "    \n",
    "    plt.subplot(1, 2, 2)\n",
    "    plt.plot(train_acc, label='Train acc')\n",
    "    plt.legend()\n",
    "    plt.show()\n",
    "    \n",
    "    return parametres"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Dataset - Test"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Dimensions de X: (100, 2)\n",
      "Dimensions de y: (100, 1)\n"
     ]
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAi8AAAGdCAYAAADaPpOnAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMiwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8qNh9FAAAACXBIWXMAAA9hAAAPYQGoP6dpAABkHUlEQVR4nO3dd3xV9f3H8dc5N4MVwghksEE2yJ6yVRAUR61bxLYOtGoVWyv211Ztrba11rrrXmitinWjqAyRvWTvPUKYSRhZ957fHycJCbkr5O77fvq4Qs793ns+ySX3fO53fL6GZVkWIiIiIlHCDHcAIiIiItWh5EVERESiipIXERERiSpKXkRERCSqKHkRERGRqKLkRURERKKKkhcRERGJKkpeREREJKokhDuAQHO5XOzdu5eUlBQMwwh3OCIiIuIHy7LIz88nKysL0/TetxJzycvevXtp0aJFuMMQERGRM7Br1y6aN2/utU3MJS8pKSmA/c3Xr18/zNGIiIiIP/Ly8mjRokX5ddybmEteyoaK6tevr+RFREQkyvgz5UMTdkVERCSqKHkRERGRqKLkRURERKKKkhcRERGJKkpeREREJKooeREREZGoouRFREREooqSFxEREYkqMVekTkTEm42H9rNozzYchsnINh3JqJca7pBEpJqUvIhIXNiTd4QbP36Db7auKz/mMEyuP3sAz467hrpJyWGMTkSqQ8mLSBxanbOHZxfN4put67CAc9t04o7+I+me3izcoQXFkZPHGfra4+zKO1zpuNNy8fbKBezIPcQ3E+7B4WMnWxGJDEpeROLMWz8u4MaPX8c0DEpcLgB25B7ipWVzefWSG7ix5+AwRxh4LyyZw47cQ7gsq8p9Tsti1vaNfLl5NRd1ODsM0YlIdeljhkgcWXtgLzd+/DouyypPXABKXC4sLH7+8Zus2r8njBEGxyvLf3CbuJRxGCavr5gfwohEpCaUvIjEkWcXzcL0smOrwzR4dvGs0AUUIvuP53m932m52Jt/NDTBiEiNKXkRiSPfbltfqcfldCUuV6UJrbEiy8eKIodh0iK1YYiiEZGaUvIiEkcML70u5W1CEEeo3dR7iNceJ6fl4uc9zwlhRCJSE0peROLIeW06kWB4/rVPME3Ob9clhBEFn2VZ9M5sSUpSLbf3m4bB2LO6cn67ziGOTETOlJIXkTjyy/4j8TxtFVyWxR39R4QqnKArcTmZ8NGrnPfWkxwvLqxyf5Lp4La+w/noqtswvSR1IhJZ9NsqEkc6pWXw1mU/w2GYJFSoaZJgmpiGwRuX3kiXJllhjDCwfv/dJ7yzajFAlbk+BgZXd+vHM+OuITkhMRzhicgZMizLy/rBKJSXl0dqaiq5ubnUr18/3OFIFMs+lsuLS7/nw7XLOFZcSO+MltzWbzgjW3f0a+5IJNtwMJtnF89ixtZ1WJbF+W07c3u/EXRukhnu0ALmWFEB6Y//hhPFRR7bJJgmeyb/laZ19V4hEm7VuX6rSJ2IG0v2buf8t54kr7CgvD7IztzDfLBuGXcNGMWTY66M6gSmY1oGT429OtxhBNXcnZu9Ji5g98Z8u3U913TvH6KoRCQQNGwkcpqCkmLGTX2G/AqJC5wadnhq4Xe8tXJBuMITPxU5nX61K3SWBDkSEQk0JS8ip3l/zVIOnMjH6WFE1cTgH/O/CXFUUl09M1pg+LHwu09mqxBEIyKBpORF5DSzd2ysNJn1dC4sVu7fzbGighBGJdXVMrURF3Xo7vG1dBgmg5q3jdnNKEVimZIXEYlZL1x0Hc3rN8Rx2vwkh2GSVqceb1328zBFJiI1oeRF5DTDW3XwWkLfxKB702bU81D0TCJHVkoDltz8AL8bOo7MeqkYQFqdetw76DyW3/p/tGvUJNwhisgZ0FJpkdMUlBTT8p9TOHzymMd5L29ceiM39BgU4sikpizLiupVYiKxrDrXb/W8iJymVkIiX1x3B/WSalUabiibO3Fn/5FMOHtguMKTGlDiIhIbVOdFxI2+Wa1Zf8dDvLj0e95fs5TjxUX0ymzBL/uNiIkidRJ8B47n89nGleQXFdKxcTrnte2Mw8tEcBHxn4aNREQCqMTl5L4Z03h60XeUuFyYhoHLsmhRvyFvXPozRrbpGO4QRSJSxAwbzZkzh/Hjx5OVlYVhGPzvf//z+ZjZs2fTp08fatWqRdu2bXnhhReCGaKISEDd+eV/eHLBN+WTvssKHe7JP8qYt//F4j3bwxidSGwIavJy/PhxevTowTPPPONX+23btjFu3DiGDh3K8uXLeeCBB7jrrrv48MMPgxmmiEhAbDtykH8vmeN2526XZeGyLP4465OQxyUSa4I652Xs2LGMHTvW7/YvvPACLVu25MknnwSgc+fOLFmyhMcff5zLL788SFGKiATGu6sXYRomTsv9Unun5WL65jUcPnmcRrXrhjg6kdgRUbPH5s+fz+jRoysdGzNmDEuWLKG4uNjtYwoLC8nLy6t0ExEJh4MnjmH6mMxtAYdPHg9NQCIxKqKSl+zsbNLT0ysdS09Pp6SkhIMHD7p9zKOPPkpqamr5rUWLFqEIVUSkilapjT32upRJME3S62oxgUhNRFTyAlXrMJQthvK0NHXKlCnk5uaW33bt2hX0GEVE3Lm2e/8qWxFUlGCaXN2tHynJqs4sUhMRlbxkZGSQnZ1d6VhOTg4JCQk0btzY7WOSk5OpX79+pZuISDg0qZvCY+f9xO19DsMkNbk2fxp5cYijEok9EZW8DBo0iBkzZlQ69vXXX9O3b18SExPDFJWIiP8mDzqfVy++geb1G5YfM4DR7Tqz4Kb7ad0gLXzBicSIoK42OnbsGJs3by7/etu2baxYsYJGjRrRsmVLpkyZwp49e3jzzTcBmDRpEs888wyTJ0/m5ptvZv78+bzyyiu8++67wQxTRCSgftbrHG7oMYhl+3aSX1RA+0ZNaZHaKNxhicSMoCYvS5YsYeTIkeVfT548GYCJEyfy+uuvs2/fPnbu3Fl+f5s2bfjiiy+45557ePbZZ8nKyuKpp57SMmkRiToO06Rfs9bhDiNojpw8zntrlrA77whN69bnqq59Sa+nYXsJDW0PICJuFTlLmLltAwdO5NMqtTHntGyHaUTUSLOEyT/nf8OUbz+iyFlCgunAadnbINw3eAx/HnWJ9v6SM1Kd67c2ZhSRKl5eNpcp337EwRPHyo+1bZjG8xdex+h2XcIYmYTbS0u/Z/LX75d/XexyAnYF4b/M/ZJaCYn8fviF4QpP4oQ+RolIJS8smc3Nn75VKXEB2HbkEOOmPs1329aHKTIJtxKXkz/42N7gsR+mk19YEKKIJF4peRGRcieKi/jtN9Pc3mdhYVkW9379QYijkkgxf9dWso95r2J+oriI6ZvXhCgiiVdKXkSk3GcbV5Ln5VOzC4sV2btYe2BvCKOSSHG04IRf7XILTwY5Eol3Sl5EpNy+/Fyfe/OUtZP4075xuu9GwFmNmgQ5Eol3Sl5EpFxWSgNcfixAzEppEPxgJOJ0SstgcPO2HrdAMA2DNg0aM6xV+xBHJvFGq41EpNxFHbqTmlzbY7e/aRj0zGhB5yaZIY7MtxPFRRSWFJNaq7aWdAfRcxdeyzmv/p2CkuJKm1A6DAPTMHn1konV/vmvydnLv5fOYfm+XdRJSuKyTj25rvsA7QElHqnOi4hU8vKyudz86VtVjpuGgWkYfDPhHoa37hCGyNz7btt6Hv3+S74pXQWVWS+VO/qPYPKg86mVoG1FgmFNzl4e+O5/fLphJRb2JWRUm078ZdSlDGjeplrP9fcfvuK+b6aRYJiUWC7K+nQy6qXy3cTJdErLCHD0Eqmqc/1W8iIiVbyxYj73fzut0sqSjo3Tee7CaxnVplMYI6vsjRXz+dnHr2MaZqVeANMwOKdFO76ecLcSmCA6cDyffcdyaVInhcyU1Go//tMNP3Lxf55ze5/DMMlKSWXzXX8myaFBgnig5EXJi0iNlbiczNmxiQPH82ndII3+zVpHVOXUnON5NH/i/vIiaaczDYNHRl3K/UMuCHFk4q+hr/6d+bu34PRyGXrvpzdzZde+IYxKwqU6128NDIuIWwmmg1FtOnFVt34MaN4mohIXgNdXzK/U23I6l2XxzKKZxNjns5hRUFLM3F2bvSYuCabJV5vXhjAqiRZKXkQkKq3K2YOB94RqT/5RjhUVhigiqQ6ny3PiWcayoMRy37Mm8U3Ji4hEpToJSfjqDDINg+QEzZeIRHUSk+jUOMNr+umyXAxoVr0JwBIflLyISFS6rHNPSrx8encYJmPP6qrJnmfIsiyOFpzgRHFRUJ7fMAzuHngungaNTAzqJiVz/dkDgnJ+iW76rRaRqDS6XRd6pDdnTc5eSk6b+2Jgz3m5f8jY8AQXxYqdTp5e9B1PLfyOHbmHARjWqj33n3MBY9t3C+i5bu4zhDk7N/HOqkU4DKN8/kuCaeIwTD68chL1k2sH9JwSG7TaSAJi+9GDPLd4Np9s+JEiZwkDm7fljv4jGdyiXbhDkxiWfSyXsVOfZkX2LhJMuyPZ6bJIcjh4/dIbubpbvzBHGF1KXE4uefc5vty8prx+C1CeWDw99mru6D8yoOd0WS7eW72EZxfP4sf9u6mVkMhPO/fmVwPPVY2XOKOl0kpeQuqbresY/+4zFDtd5as/EkyTEpeLh0dczO+HXxjmCCWWuSwX32xdz8frV3CypJjuTZsxsecgGtWuG+7Qos4LS2Zz++fveB7KMQy23PVnWjdIC2lcEh+qc/3WsJHUyKETx7jkP89RWOKs9EmtbC7CH2Z9Qt+sVgHvbhYpYxomo9t1YXS7LuEOJeo9vWim1/sN4KWlc3nk3EtDEo+IJ5qwKzXy2op5FJQUV0pcKnIYJk8s+CbEUYlIdVmWxfqD2R57XQCclsXqnD0hi0nEEyUvUiNzdmzyugux03IxZ8fGEEYkImfCMAySfazMchgGtROTQhSRiGdKXqRGIqvmqojUxGWde5VPfHbHaVlc2qln6AIS8UDJi9TI8NYdML1UCnMYJiNadQxhRCJypn496HzA/YeSBMOkXcMm/KRzr9AGJeKGkhepkRt7DqZ2QpLHBMZpubhn0LkhjkpEzkSvzJZMu3IStROTMLBXDZb1xLRr1IRvb7hHRf8kImiptNTYzG0buPCdpylyOqsslf7LqEuZMlSFwkSiSW7BSd5euYAl+3aQ7Ejkog7dGXtWNxxehpREakp1XpS8hNyu3MO8sGQOH5cXqWvDL/uNZEDz+NiX5HhRIVNXLeTd1Ys5cvIEXZpkckufoQxv1SHidmMWEYlESl6UvEgI7Th6iJFvPMH2owcBAwurvOfppt5D+PdF12Ea+sQqIuJNda7fekcVqQHLsrj4P8+xK+8wFpTXuykr0vfysrk8vdB74S8REakeJS8iNTBnxyZW7t/tdXfjx+d/jdPL/SIiUj1KXkRq4Ntt673WxQDYnXeUbUcPhigiEZHYp+RFpAacLheGH6X6vFUhFhGR6lHyIlID57RsR7HL6bVNWp16tNEuvCIiAaPkRaQGxrTrSpsGaTg8rCYyMLiz/0gSHY4QRyYiEruUvIjUgMM0+fjq20mtVRtHhXouZRWHx7XvypQhKtInIhJIqvMsUkPd05ux5vY/8vzi2by9aiF5hQV0bJzObX2Hc1W3viSY6nURcedowQn+u2YJu3KP0KRuCld17Ut6PdXnEt9UpE6qbWfuYRbv2Y7DNBna8iwa16kX7pBEJMo8tfA77pvxIUXOEhJMB07LhWkY3Dd4DH8edYkqU8eh6ly/1fMifss5nsctn77NJxtWlhdjSzQd/KzXYJ4ccyW1E5PCHKGIRINXl//Ar6a/V/512aR3l2Xxl7lfkpyQwB+GXxSu8CQKKHkRr1bn7GH9wWxMw+T+b6ax7cjB8sQF7Dedl5fNZduRg3x53V3auE1EvHK6XPx+5sde2zw2dzr3DDyPlORaIYpKoo2SF3Fr5f7d3PTJWyzeu91nW5dlMWPrOqZvXsOFHboHPzgRiVoLdm9lb36u1zYnS4r5YtMqrurWL0RRSbTRx2SpYsPBbIa8+neW7dvh92MchslrK+YFMSoRiQVHC0761S630L92Ep+UvEgVf5j5CSeKi3BWYy6303KxK+9wEKMSkVhwVqMmfrZrGuRIJJopeZFK8gsLmLZ+OU6rehsJOgyT5ikNgxSViMSKjmkZnNOincfCjqZh0Cq1ESNadwhxZBJNlLxIJQdPHPO6Q7InTsvFjT0HBSEiEYk1z114LbUTE6skMA7DwDQMXr1kIqaH5EYElLzIaRrXqevxE5EnpmEwonUHxrXXZF0R8e3s9OYsvOl+xrXvVmlj06Gt2jPnxl8zqk2nMEYn0UCrjaSS+sm1ubRTD/63/ke/ho4STJPruw/gmXHXaJm0iPitS5MsPrnmlxw4ns/e/KM0qZtCVkqDcIclUULJi1Tx8MiLmb55DQUlxW4n7d7UewijWnckwXQwrFV7lfMWkTPWpG4KTeqmhDsMiTJKXqSKLk2ymH3jr/n5J2+ycv/u8uP1kpL57TljeGDoWI1Hi4hI2GhvI/HIsiyW7dvJuoP7SEmqxXltO1M3KTncYYmISAzS3kYSEIZh0CerFX2yWoU7FBERkXJKXkRERDzYeuQAzy2ezccbVlBYUsLA5m24o/9IhrVSHZpwUvIiIiLixtdb1nLxu89S4nKVr77ctz6X99cu4/fDLuThkReHOcL4pVmXIiIipzl04hiXvfc8xU5npbIRZUU8/zTncz7d8GO4wot76nkREZGYtXL/bp6YP4MP1y2noKSYzmmZ3Nl/JD/rNZgE0+Hxca+tmEdBSTEu3K9pcRgmTyz4hvEdewQrdPFCyYuIiMSkLzet5pL/PIeFVd5jsjpnL7d89jafblzJtKsmeUxgvt+xCZeXxbhOy8UPO7d4vH/5vp18sWk1Rc4S+mS1Ylz7bl6TJakeJS8iIhJz8gpPcsX7L1LicmFV6D0p+/tnG1fx9MKZ3DPoPLePNwx744Lq1hI5dOIYV77/It9t34DDMDEMe6ipWUoDPrxyEgOatznD70gq0pwXERGJOVNXLuJEcWGlxKUiC4t/LfwWT6XO7F2tDbf3gT1sNLJ1x0rHnC4XF7z9FLN3bLK/tlzlPT7Zx3I5980n2Hw45wy+GzmdkhcREYk5S/bu8Lnf2o7cwxwtOOH2vok9BlEvKRnTcJ/AOC0Xk0/rtfli0yqW7Nvhdl84p2VR6Czhifnf+PkdiDchSV6ee+452rRpQ61atejTpw/ff/+9x7azZs2yu+tOu61fvz4UoYqISAxIcvg3vyTJ4X72RMPadfns2l9SKyERR4UEJqE0IXrs3MsYc1bXSo95b80SHF62TilxuZi6aqFfcYl3QZ/z8t5773H33Xfz3HPPcc455/Dvf/+bsWPHsnbtWlq2bOnxcRs2bKhUHrhJkybBDlVERGLE2PbdeGHpHI/3OwyDgc3bet3yZFirDmy680+8uPR7Pl6/ggJnCYOat+X2fsPpm9W6SvujBSfd9rpUdKyoEMuyMDz06Ih/gp68PPHEE/ziF7/gpptuAuDJJ5/kq6++4vnnn+fRRx/1+LimTZvSoEGDYIcnIiIx6ML23WnfqCnbjh4sn3dSkdOyuH/IBT6fJyulAQ+OGM+DI8b7bHtWoyYkmKbb85Vp3aCxEpcACOqwUVFREUuXLmX06NGVjo8ePZp58+Z5fWyvXr3IzMzk3HPPZebMmR7bFRYWkpeXV+kmIiLxzWGafD3hV7So3wigfO6KwzAxgH+OuYKLOpwd0HPe3Huo18TFNAxu6zs8oOeMV0HteTl48CBOp5P09PRKx9PT08nOznb7mMzMTF588UX69OlDYWEhb731Fueeey6zZs1i2LBhVdo/+uijPPTQQ0GJX0REolfrBmms++WDfLB2Gf/bsILjRUV0b5rFzX2GclajpgE/X9emWdx/zgU89sP0KsusHYZB78yW3N5vRMDPG48My9M6sQDYu3cvzZo1Y968eQwaNKj8+COPPMJbb73l9yTc8ePHYxgGn3zySZX7CgsLKSwsLP86Ly+PFi1a+LWltoiISCBZlsVLy77n0bnT2X70EAD1kpK5qdcQ/jTqYuol1QpzhJErLy+P1NRUv67fQe15SUtLw+FwVOllycnJqdIb483AgQN5++233d6XnJxMcrLnCVfx5nhRIe+uXsy8XVswDYNRbTpxeedeJCckhjs0EZGYZxgGt/QZxk29h7DpUA5FTiftGjWhTmJSuEOLKUFNXpKSkujTpw8zZszgsssuKz8+Y8YMLrnkEr+fZ/ny5WRmZgYjxJjy/Y5NXPyfZzlacLJ8Od8ry3/g1/VS+er6X9E9vVmYI5TwKAAWAkuAk0AmMBRoj7ciXCJy5kzDpGNaRrjDiFlBX200efJkJkyYQN++fRk0aBAvvvgiO3fuZNKkSQBMmTKFPXv28OabbwL2aqTWrVvTtWtXioqKePvtt/nwww/58MMPgx1qVNt25CAXTH2KgpJigEqTxnKO5zPqzSfYeMfDNKxdN1whSlgcBP4BHK5wbA+wCBgGXIsSGBGJNkFPXq666ioOHTrEww8/zL59++jWrRtffPEFrVq1AmDfvn3s3LmzvH1RURG//vWv2bNnD7Vr16Zr1658/vnnjBs3LtihRrVnFs2ksKTE7UZiTsvFoRPHeX3FfI/7eEgs2oOduBw/7XhZYjsHyAJGhjIoEZEaC+qE3XCozoSfWNLqySnszD3stc2g5m2475wLyDmeR/P6DRndrot2OY1JLuAdwHMl61MaA39GO4WISLhFzIRdCZ2TxUU+2yzas53L3nu+/Ov0uvV5euzVXNG1TzBDk5D7BP8SF4BD2ENKacELR0QkwPRxK0b0yGjhdU8NsCtKVrT/eB5XffAiH69fEcTIJLROAtXd+M17OXMRkUij5CVG3NFvhM89NTy59+sPPG4LL9FmA1BcjfYp2ENHIiLRQ8lLjLi4Yw9+0escAIwKq0d8rSOxgC1HDrB47/agxSah5Hv48BQDGAVo3pOIRBclLzHCMAxeGj+Bl8ZPoHOTU7UFWqY28uvxOcfzgxWahFR1avmcDYwJViAiIkGjCbsxxDAMbuo9hJt6D+FYUQGmYTJr+wYufOcZn49tXr9hCCKU4GsGtAF24HkuSy3gOqAv+vwiItFI71wxql5SLeokJjG6XRea1k3x2M40DLo3bUaP9OYhjE6CayJ2gnL6r7cJ1AUeAPq7uV9EJDro3SsKHTxxjB1HD1FY4ntiZoLp4Jmx12BQdf6LaRiYhsHTY6/GMFRlNXZkAr8DBnOqczURGICduPi/r5iISCTSsFEUmbFlLQ/N/owfdm0BTu1U+ofhF3ot+39F1z4kOW7j3q8/YMuRA+XHuzbJ4umxVzO8dYegxy6hlgZcj71/0VfAXmA+sAU4HxiCPruISLRShd0o8fbKBdzw0WsYhlFpCwCHYdK+cVPm/fw+n/sWWZbF4r3byTmeT4v6DTk7vbl6XGLaR8B07D6303/NB2EPL+n1F5HIUJ3rtz56RYGjBSe45dO3saDK3kVOy8WmQzn8ec4XPp/HMAz6N2vDRR3OpkdGCyUuMW0bduICVRMXsHthfgxdOCIiAaTkJQpMXbmwfLdod5yWi5eWfU+RsySEUUlkm433X28TmBWaUEREAkzJSxRYfzDb5waK+UWF7D+WF6KIJPLtxHvZfxewO0SxiIgElpKXKFAvqRaW267/yuomJYcgGokOtfxoo38vIhKdlLxEgZ926U2Jy/OnaIdhMLxVBxr5mLAr8aQ33ifjmqVtRESij5KXKNAnqxVj2nVxu2u0Abgs+MPwC0MfmESwwUA93P+KG9h1X0aGNKLQ2QOsAXbhfrKyiEQ7JS9R4v0rbuX8dp0BSDBNEk0HBlA7IZF3Lv8Fo9p0Cm+AEiRnevGtA0wGUku/Njn1614H+BXg375X0WMj8GfgYeCp0r8/BKwOZ1AiEgSq8xJllu3byYdrl3GsqJDOTTK4tnt/6ifXDndYElAngG+B74Fc7GRjEHZxueruQeUElgMbsCfpngX0AZICFWyE2AA8iZ3sVXxLKxs6ux17I0oRiVTVuX4reRGJKHnA34EDVL4Im0Bt4DfY5f/lFAt4ENiP556qhsBfUGezSORSkTqRqPUecJCqF2EXcBJ42c198W4HkI33n8sR7GGlM3W49Dy5NXgOEQkU7W0kEjHygGV4rs9SVptlO9DGQ5sC7KGSeFoGfTjA7SrajL3NwuYKx7oClwPNzuD5RCQQlLyIRIw9eC8sV2YHlZMXC/gB+AbYV3qsFTAa6BvIACNUSoDblVmHPfH39B6ddcBj2EN4Lav5nCISCBo2EokY3qson1LxM4cFvFV6y65wfCfwEvBxYEKLaO2ABj7a1AVOX5FnAVuBucAi4FiF+1zAm1SdAFx2Xwnwrh+xlQ33+ZOUioi/1PMiEjHaYE/KPemljYE9bFFmJXavC1S+yJb9/QvsVTaehpligQn8FHs+kCeXYde2KbMLeBXYW+GYAxiBPSS0Ee/DTC7sxGcf7idQ52BvjLkQO9GpBZwDXABoIYFITannRSRiJGIvh/bEAPpRebn0LHxvwDinxpFFvn7AjdjLyiuqBVwLDK1wbD/wOJV7qsBeVv4tdi/WAT/Pe9DNsZ3AI9g7d5dtlloAzMRe8XTEz+cWEU/U8yISUcZif+Kfi514uCr82Qm4/rT2u/C9AePOwIcZkQZhz/FZg50g1Ae6U7WmzedAEZ5/bvPxfzLu6TWWLOAVoBD3w0252MNNt/v5/CLijpIXkYhiAhOAYdjDQYexJ5oOADpSdb8if4rNxVpBOm8SgZ5e7i8GluA94TOxk5/E0vaeNADannZsC1V7dCpyYQ/1HaH6BQdFpIySF5GI1Kr05ktv7KEOTxdjA23AWFEh9vCQLyext0/Y76XNQKoO2e3247kt7LkySl5EzpSSF5GoNgKYjd1DcPowRVlV3sEhjul027ATrDXYMbYDzgW6hCGW2vjuUQH7Z+ctcQGYB1xE5YnAiR7ani6eesNEAk8TdkWiWhr2Jotlcy8qbsCYgr05Y90wxFVmHvBXYCn2nk0ngbXAv4BPwxCPA3tujLe3Phf2aiFf8oA3TjvWzcdzg/16tPbj+UXEE/W8iES9s7CLpi3GnnNhYE/u7YX/PQHBkIP7WillQ1yfYcfeOcRxjcPerPI47ofbzsPuzfLHYuBS7CQS7N6ahsAhL4+5AL31itSMfoNEYkIyMKT0FilmYydSnvYcMrGXD4c6eWkI/BaYil0tt0wd7MSiBXa1Yn8tx17iPh17K4HTJ1XDqZ/DeXhfDi/xwLIsNh7aT27hSVo3aEzTuqr9U11KXkQkSLbgexn3Zi/3B1MT4G7sei7Z2HNQ2mL3VC2txvM4sIfCNmInLuA+WbOAu6hcYFDi0cfrV/C77/7HmgP2Vh6mYXBJx578Y/RPadMwzcejpYzmvIhENQv3k3UjgT9vL+F+C2qCXQumI6eG2BpX4/FOIB34Dt/FAteeSYASQ15fMY9L33uetQdOLad3WRafbPiR/i8/yrYj7ooeijvhfucQkTOSC3wA3APcgd2L8B6RVb21G+6HUMqY2IlDpGmFXfLfW+xlamMvRd+M716mjTUPTaJWfmEBv/zC3g/LOu3DhtNyceTkCaZ8+5G7h4obSl5EooqFPSn0Eezlx2X7IBVgbxXwZ3wv8Q2VIdjDMd6SgFEhiqU6DOxKxt7eHo3S20TsHht/Eh293caz99cu5WRxkcf7nZaLD9ct48jJ4yGMKnppzotIVMgDZmBvG3DCQxtX6X2vY09IDbf62L1Cz2CX4y/7tFl2Ef8Z9uTYSHQW8Gvs3q0tHu4fjz3cBHYv0wK8FwvsFuAYJdJlH8vlucWzeWvlAvbl5/oc3C1xudidd5SGtcNZ3iA6KHkRiXhHsGul5OJ9aAJO7Xa8G2ge5Lj80QG7N+gH7DkfLuwL/1BOLS+OVG2B+7An9eZir+hKwh4qOn11yCjsPZE8cVB5c0iJdesO7GPY649z5OQJnJav39tTGtY+fXNRcUfJi0jEm4p/iUtFu4iM5AXsC/3Y0ls0alJ686YF9q7Wb1C1rg3Yu0u/jz0cdfpmjhJrLMvisveer1biYhoGA5u1oXl9bRvhDyUvImFTgr1J317sT/U9sFe6rACWYc9jSQVWncFzh7M4XbwaiD3R9x/Yeyidbil2L9q92D0xEqtmbd/IhkP+zz0rmzH18MiLgxNQDFLyIhIW64BXgHzsOSAW9vyKWthJS1lRM38mgp7OQegLv4ltDfb8Hncs7Pkzq/C+87VEu/m7t+AwTJ+9Lg7DwGlZNKhVh5cvnsC5bfV76y8lLyIhtx14mlPDQBXf4ApK/7RO+9NfBjCM8O5nFM9+wPtrZmLPjekZkmgkPBxG2QcS727rO5yhrdpzScceJCeot7Q6lLyIhNwXuJ8XURMmdhLUB/hpAJ9XqnJiD+1txE4WO2AP+Tmwe9K8cWHPX5JYdn67Ltzvo2ZL07opPDHmShIdGkI8E0peREKqCHueSyATlzTsZbiDqNluxcXADuy5OM2wd6WWynZhL/0+yql5KzOx90u6s/TPbLePtJlUr4KvRKPemS0Z0qIdC/Zso8RVdejIACYPPE+JSw0oeREJqQJqlrgYFf50Ye8c/QtqNkHXhb2p4AxO1ZAxgb7AVUC9Gjx3LMkDnuDU0J6zwn252BN1zwU+xfNr7ALOCVaAEkHev/JWRr3xBOsOZmMaBi7LIsE0KXG5uKHHIH5zzuhwh1htmw/n8Pzi2Xy/czMJpskFZ3Xl5t5DyUxJDXkshmVZkbgpyhnLy8sjNTWV3Nxc6tcPz06dxU4n769dysvL5rIj9xCZ9VK5sedgruven9qJSWGJSSKFE7ukv7vVKL7UAQZgXyhTONXTciaTeit6G/jezXETe4nw/aXnjnefAp95ud8ALsJeVZRN1aXtBnA2cBvuXzMX9mTeRdjDT02wqxS39tBeIl1BSTHvr1nK1FULOXzyOO0bp3NL76EMa9Uew4iu1/SNFfP5+SdvYGCUT0Q2DYNajgQ+vfYORrXpVONzVOf6reQlwI4XFXLB1KeYu3NzebZtYuDConvTLL6beC9pdfRJNr79F3uooTp1WwxgHBDopZQ7gL/4OO944MIAnzfaFAOT8bySqEwL7OT0P8ASTr3GicAI4FLcd3ifBJ7CLjBYNn+p7M/BwAS0vYCEy9K9O+j30qNV9mSC0gQmIZEtd/2ZjHo164GpzvVbw0YBds9X7zNvl11O3FWaF7pKX/C1B7L52f9e59Nr7wjY+Y4WnOD1FfOYtm45J4qL6JXZktv6Dqd3ZsuAnUMCbRzwI3AY9wlM2TLpitoTnCJv8zh1kXTHAuag5OUzfCcuYA8p1cUeyrsC2In9822LvQzek9ewV6FB1VVo87B7YcZVK2KRQHly4bc4TIMSV9XkxWVZFJQU8/KyufzfsNC9Tyh5CaDDJ4/zxo/zypOW0zktF59vWsWWwwdo18hXxU7fVu3fw6g3n+DQiePlGfGP+3fz8rK5/GnkxSH9hyTVUQ9776GPgIWcmjuRjl1Cfj+wGPti2RT7E/sQglN47hC+e4COcuY1Z2JBMfaml74YVK5qXB//9jPaj53MevMNMBq9ZUs4TN+8xu3E4zIuy+KrLWuVvESrRXu2UeR0em1jAd/v3FTj5KWwpJgxb/+LIydPVOrKK/sH9vuZn9C1SRaXde5Vo/NIsNTH3pH4CuAg9p456ZxKEK4PURz18N7zAvZ8l3hNXAByODVJ1xsLGH4Gz78W971tFR3HXunU5gyeX6RmXH5sceD0ktwEgwZRA8jf2UOBmGb0wdpl7DuW67GCo8Mw+Pu8r2t8Hgm2OkBLIIPwJAgD8J64mNgTg+OZv69La+BMJi16/8BT/XYigTWkZXsSDM/pgsMwGdryrBBGpOQloPo3a02i6Xvd/tBW7Wt8rm+2rSPB9PzyOS2L+bu3UlhSXONzSSzriH3BdXeBNrHnaZwX0ogiTwb2HlO+XMWZJaCt8b183oG9b5JI6P1qwChKfPS+3Np3WIiisSl5CaDGdeoxsecgHB6WwDkMk3FndeOsRk1rfC6ny/Krp8cZW4vJJOBM7KW7falcQwbsi/ZvgEZhiCuSmNjzTbzd34YzH9JpB2Th+e3YxN70UVs+SHiMatOJP5VuGlnxQ3OCaWIaBm9e9jPaNqz5PM7q0JyXAPvnmCtYm7OPebu3lC+VNkrHszumpfP6pTcG5DyDW7Tl7ZULPN5vYNClSSZ1VFdGfKoF3AT8BFiNPTzREnuFTDzPdaloFLAPmMupOUJl81Sa4Ll2iz8M4Bbg79hLpl2n3ZeFtnyQcPu/YRcypOVZ/Gvhd8wtL1LXjV8NGEXPjBYhj0d1XoKgyFnCe6uX8NKy79mZe5j0evX5ec9zmNBjYMCSifzCApr/87ccKyr0uLrplYtv4Oe9VM1TJDDKdoX+HnuFUB2gP/Z+UoFYCXYE+A5748YT2D1ew0pv3pZZi8QGFakLc/ISKjO3beDCd56m2OksH48s24b9F73O4aXxE6KuiqOIiMQnFamLEyPbdGTVbX/kmUUz+WDdMk4WF9EzowV39B/JJR17KHGJaUextxhoiL3MWkQkfoSk5+W5557j73//O/v27aNr1648+eSTDB061GP72bNnM3nyZNasWUNWVhb33XcfkyZN8utc8dTzIvFoFXa11+2lXydgT+a8DG2gKCLRrDrX76CvNnrvvfe4++67+d3vfsfy5csZOnQoY8eOZefOnW7bb9u2jXHjxjF06FCWL1/OAw88wF133cWHH34Y7FBFgug48DkwBfgl9maHnwLHqvEcPwDPcCpxASjBnkT6f9hzJkREYl/Qe14GDBhA7969ef7558uPde7cmUsvvZRHH320Svvf/va3fPLJJ6xbt6782KRJk/jxxx+ZP3++z/Op50UiTy7wN+xS/BV/3QygAXAfvpcjHy9tV+KlTQPsTRZ91xoSEYk0EdPzUlRUxNKlSxk9unKNhNGjRzNv3jy3j5k/f36V9mPGjGHJkiUUF1ctuFZYWEheXl6lm0hkeQt7E8bTPydY2InN6348xyK8Jy5gz4PxneCLiES7oCYvBw8exOl0kp6eXul4eno62dnZbh+TnZ3ttn1JSQkHDx6s0v7RRx8lNTW1/NaiRejXm4t4dgh7noqn6pQuYAP20ltv3P++VDXbz3YiItErJBV2T1/1YlmW15Uw7tq7Ow4wZcoUcnNzy2+7du0KQMQigbIjQO1q+/k8kT7v5RgwA3i+9PYddmE2ERH/BXWpdFpaGg6Ho0ovS05OTpXelTIZGRlu2yckJNC4ceMq7ZOTk0lOTg5c0CIB5e/8E1/t+gBf+vE8kbziaC12wlKMPWRmACuAj4E7gdBu7OZZEbAYe6juOPaeQkOB9vhfRbcAWAiswR7ua136HA0DHKtIfApqz0tSUhJ9+vRhxowZlY7PmDGDwYMHu33MoEGDqrT/+uuv6du3L4mJgahiKRJK7fH9GcEEOvho0wJo7sf5InUH6APAs5xKXKjwZyHwFPb8n3A7AjwMvIk9nLcLWAL8A3gb7ztwl9mFvfrrHWAldgLzBfAA4HlLDxHxX9CHjSZPnszLL7/Mq6++yrp167jnnnvYuXNned2WKVOmcMMNN5S3nzRpEjt27GDy5MmsW7eOV199lVdeeYVf//rXwQ5VJAjqYH/i9vSJ3QAGAyl+PNfdgKdeRgP7U/2QasYXKrOxL/zuFjda2L0d34c0IvdxPIc9T6nsaziVsMzFHubypgB4klNL4Csmai7sydnb/IznODAdeBD4NfBX7AnZTj8fLxK7gl5h96qrruLQoUM8/PDD7Nu3j27duvHFF1/QqlUrAPbt21ep5kubNm344osvuOeee3j22WfJysriqaee4vLLLw92qCJBcjn2aqMfObWpX9mfXYCr/HyeFOBPwNPYn+4ragFcjT1UcQJoCvQkcqrvLsd7r4WF/fO5KDThuLUFcF9/6pQZ2Js0evrctxDvtXuM0ue4xcd5DgCPY/dGlSVAx4Ct2AnMnQRmPyWR6KS9jURCwgI2AvOwhyZSsYd4OnFmHaC7gPWlf2+DPT9jTunXJvan89rABOz5MuH2W+yl3N5kYvcyhMun2MM7voaGHgIyPNz3LPbqMm9vq0nYCagnFvAIsMdDLAZwHtppWmKN9jYSiTgG0LH0FggtSm8AUzmVuMCpYYWTwEvYSUyXAJ33TLXB7lnxlBiYpW3Cyd/Pcd6SmxI/nsfXsM9WqvasVWRhv97j8TyMKBLbQrJUWkSC5TC+54p8HIpAfBiJ94u+Cxgeolg8aYvvXpc62ENynrTB+4okA2jl4xxbfDwH2JOc9/poI/Ekv7CAowUniLHBFI/U8yIS1Zb6uN/C3gvpML63IAimjsBY7OXeBqd6J8rm/lyGvZw4nLoAadg/K0/DNSPx/rY5BPt79HQBsbDnzHjj73Js7Rov8MHapfzth69YvNeuFdWuYRPuHngut/UdjsOM3f6J2P3OROLCCfz7NT4e7ED8cClwG3Y9F5NTS8TvBC4IX1iAvYR7LTAMeyimYmJQ9vfO2AmYN42AiaWPqfi6lD3HUKCvj+foiO+hpyTsJdiLsFdqSTz60+zPueL9F1m679RE861HDnDXl//h+mmv4LL8WdofndTzIhLVmuJ7DoVJeHtdKupZeiu7OIe798ACvsXe8ftEheMp2D+3YiAde0irP/4VHRxY+pivgdXYr08r7B6Xvvj+nlsC7bCXVHu6+BQBn5XeXwt7pVmk1viRYFi5fzd/mPUJAK4KQ0Vlf/vPmiVc0qknV3frF4bogk/JSwQrdjr5cvNqth89ROPadRnf8WzqJ/tbJl7iQ2/gXew5EO6YpW3qhiwi/4Q7aSnzJe7nBB3Dfnu8H/+KA56uDXBrDeK6Fbsw3n5ODbNVHG6DU4lNAXb9mGTs11riwQtL5pBgmpS43Ce4DsPg2UUzlbxIaE1bt4xJn03lwIljmIaBy7KonZDI74ddyP1DLvC6N5TEk2TgGtzvTG1iTzC9LJQBRZF87OXR7ljYPSYfYQ9rhVoqdpXeRdi1Y/LwvTnnR0AvIicxlGBakb3LY+IC4LQsVubsCWFEoaU5LxHoy02r+el/X+TgCbvYVVmX4MmSYh747n/85Xt/9riR+LAKe/jgdAb28MwU7EmoUtVivM8tcWEP++SEJpwqkrAnAN+Lf3OCcoDdQY1IIkfdpCSfaWrthEgpUhl4Sl4ijGVZ3PfNh/bfPbT58/efk1ugnXhlNXZRtENu7rOwhxCUuHiWi39vgU/ivWpuKBzHvx6VSJiYLaFweWfvQ4QJpsmVXSOhQGVwKHmJMOsO7mN1zl4sL58IC0pK+N/6FaELSiKQBfynwt/d+S/aB8ebVPzbaPEw4a+V0wT/iug1DnYgEiGuP3sAmSmpOIyql3HTMEgwHdw1wNey/Oil5CXCHDju+xOewzA5cCI/BNFI5NqOvf+NtwtaHrAuJNFEJ39W/oD9M56HPTE2XLoB9bzcX7bsvElowpGwq5dUi+9umEyL1IaA3dOSUFrXJSWpFl9edydnNfJWUDG6acJuhGmZ6ntJq9Ny0bqBPmHFt9wAt4tH9YEL8Txpt6IS4CBntvIoEBzA9cC/S7+umLSa2G/lV4Y6KAmzjmkZbLrzT3y8/kdmbF1HicvJwOZtuaZbP+omxfbWEUpeIkybhmkMa9WeH3ZuwemhwFCDWnUY3+HsEEcmkcXfTUdTgxpF9LsQu57Kaj/ahnsX517AXcA0Ku991BG4AmgWjqAkzBJMB5d36c3lXeJrmbySlwj0zzFXMuTVv1HktHBWKD5UVuXhmbFXk5wQ7jdSCa822EMEB7y0ScGuCiueGdi7M/tKXtLxvqdRqHQpve3HnkTckMgpQCgSOprzEoF6Z7Zk7s/vY2DztpWOn9WoKdOunMR1Zw8IU2QSOQzsYQJvczZ+in8VYeNdJtAD7z/LC33cX13bgQ+At7Ar8eZV8/Hp2FV4lbhIfDKsGNuCMi8vj9TUVHJzc6lf39+u9ci1+XBOeYXdnhktVJxOTvMj9qqjwxWO1cceRujv47FHgQWlj61X2j4j8CFGhQLgBewJzmWf6creGi8DxgTwPC9i70tU8TwmdjI6IkDnEYk+1bl+K3kRiXouYBNwBDtx6Yj3HhcL+IJTE1XN0mMuYDD2xNB47LGxgK3YxetOYg/LnYM9NBMozwEr8bxKbBL23BZvMR4v/bMeqqYrsaQ612/NeRGJeiZ2wuKvOcAnFb6uWAtmHvaWA1cHIK5oY2APxbQL0vPvxe4p83b+z7ArI5+elFjAD9hDTPtLjzUBzsfeCVtJjMQXzXkRiStO7B2UvZmNve+PBNZyvL/lWtjl/Y+4Of4u9vyY/RWOHwDeAd7GvwJ2IrFDPS8icWUHvmu/uLCHNs4JfjhBcwJ7Ps927IShK/ZwTDjf8orwr4fk9GJ4G7ATSk/mYn9v3c4wLpHoo+RFJK4U+tHG8LNdpFqNXcytiFM9HfOx5678Cnt1UThk4nu7hkSqlvifhf19eNrKwCxto+RF4oeSlwix7sA+nl8ym7k7N5PocDDurG7c3GcoWSkNwh2axJR0P9pYhO8CX1N7sSfFll3oK17wc4F/Ag8DtUIcF0Af7JVhBbgf5jGxJ0yfXhl1D973YHKVthGJH0peIsCLS+cw6bN3cBgGJaVVdZfs3cHf5n3NZ9fcwcg21ZmMKeJNI+xP6Gtxf0E0SttE67+5b7ETA3fJgQs7gVmEPck11BKBnwPPY/+cK/78TewJuBe7eZw/Zd5juxS8yOk0YTfM5u/awqTPpmJhlScuAC7LoqCkmIvefYac49UtYCWBVQIsxP7U/ofSPxeWHo9G1wB1qfrrb2Ivkf65m/uixXK891IYwIrQhOLW2cBvsKvklqkFjAJ+i/vNF31tIGmUthGJH+p5CbN/LvgGh2lS4qr6hluWwLyy7AemDB0bhujErvfxL+z9b8o2aMgB1gMzsedQ1A5bdGcmDXgAe1luWRJmYFeZvRBoEb7QaqzYx/2WH22CrS1wJ/bwURF2Iumtrs4Q7CXSJ6mamJnYyU84epJiX0FJMQt3b6PQWUL3ps3ITNFeYZFCyUuY2TuBev6k6LIsvtm6TslL2LyDvUIHTg1FlP25o/T+X4Q6qABoBNyA3QtzDDsBC/U8kAJgC3by1ILAlLpvjp1oelo6bBA5VYRr4d/PvB5wL/A09jLqskTHib1/1Z34v1Gn+MNluXj0++k8Pn8GRwtOAGAaBpd26smz464ho56SmHBT8hJmLj8KHDtjqwhyFMkFluB5GMJVev9Pid7dmxMJbAVZfzixq/t+i93zAHZS0R24DmhQg+ceiV0l1xMLu7dpAHBWDc4Tas2AR7CHvDZifx8dsJdIx2M15OD65Rfv8sKSOZWOuSyLTzb8yLJ9O1ly8wM0ruNuiE9CJVoHtmPGkBZnkWB4fhkchsGwVu1DGJGcshnv8ycovX9zCGKJFRbwBvAlpxKXsuOrgb9i9wSdqb743tOpCHiK6m+GGG4O7BVL1wDXYn+vSlwC7cfsXVUSlzIlLhe7co/wzwXfhDgqOZ2SlzC7e+C5lSbqVmQApmFyc+8hoQ1KJGi2Y/d8uOPCHhb5tgbPbwI/A7wl/BZ2AjO3BueRWPXainkkmJ4vjU7Lxb+Xfh/CiMQdJS9hdn67Ljw8wl4eWfEXJsE0cZgm71z+C1qkatv78GiL74qoRmk78c88fJfIr+mFwQQO+mhjYVcRFqlsV94RnF7mIQIcPHGMYqevgoMSTJrzEgF+P/xChrVqz9OLvmPuzs0kmA4u6nA2d/YfSdemWeEOL441xJ5TsAL3w0cm9iZ6oZ4zUqYYe9JwCZBFdEzaPIrvobh87OSiJpsN+rOMPVqXukswNamT4nEFaJmUpFpee2ck+JS8RIjhrTswvHWHcIchVVyPvTR6N6eWSpf9mVV6f6i5gOnADOw9fMBOpPoAV2GvQIlU9fFe6h7sn++LwHDsYnlnksS0BVZ5OY+JeszEnevPHsC/l7qf8wJ2r/iNPQdhGNrJO5yUOop4VRe7eNj1QCvslTCtsFfF/Lb0/lB7B/iYU4kL2BfppcDfTjseaQbgu+fFwu7t+id2Of0zWW030sd5XNjJkUhl57Rox4Xtu2G6SU4chklKUi3uHXR+GCKTipS8iPiUBAwFpmCvhpmCXRQsKQyx7MTznBAXcAD4LnThVFt77Cqzvj61liUes7DnyVRXZ6CsNlLFt7myv1+NvfxYpDLDMHj/ilu5ocdAHKUJTNm/1q5Ns/j+Z7+hVYPTN8+UUDMsK7aKiOTl5ZGamkpubi7160fDHACR6ngXmIP3XoVU7B6YSFWM3aMyD9+9MAb2ZpIPcmbDRz9ir17aUvr4TsD5RO/eTRJKe/OP8vWWtRSUFNMroyX9m7XWcFEQVef6rTkvIlHlML4v+LnUfMJrMCUCE4BLgFeADXgeGrKAbOyhsDMZoutRehOpvqyUBtzYc3C4wxA3NGwkElXq4fvXtjaRm7hUVB9oin+xxlQHsYjUkJIXkajSH+89LyYwKESxBEJ7fPckpROeidEiEqmUvIhElU7Yk1Hd9VaU7TAc7JUQBdi1WHwlHf7oxanl056cT3T0JIlIqGjOi0hYbceu1/Ij9oaFzYBzsZcUu7ugG8BtwFvYm0JWHE7JAG4mMLszu7MWe0+ijaVf1wdGAKOx57H4o6ws/xzs+Tv1sOekLMFOisq+n7JaMEOBSNke4xD21gZHsSdFDwDSwhmQRIFjRQVMXbmID9YtJb+wkO7pzZjUZxh9slqFO7SoptVGImGzFHi59O9lvRhlBfD6AT/He4/EYWANdqXYlvi3ncGZmoudMJXFV8YA2gF34zuBOQn8A7vg3+nPUQ97SGwtdoLTHLsOSxfC3+tiAR8BX5fGUvYzcAGjgCtQJ7a4s/lwDiPfeII9eUcAAwuLhNLqvQ8MGcufR12i1UsVaLWRSMTLw15pc/rQS9lFfTH2EJG3XodG2D0TwZYLTC39++mfdSzsZcjfAhf4eJ7/Ans8PMfx0ud5sCaBBslXpTeoGvt3QB1gfEgjksjndLkYN/Vp9uXnlv6rsf9ftu3AX+Z+SecmGVx/9sCwxRjN9HFBJCx81TgxqNnuyp64sOusVMc8vK/2sYCZPtocxx5y8fQ9u7CH0HZUM7ZgK8IeKvPma+whL5FTvty8mk2Hc3Ba7v/Nmxj89YevCPTgx4aD2dz66ds0/OvdJP/5ds5+/mH+vWROzG0kqZ4XkbDwdZG2gL3Y82AcATjfLuz9kJaXPmcD7Pkq5+K7UvBeP57/KHZS5Om5dpee15et2NsvRIqN+E5MioD12Jt0ithmbFlHoumg2OX+370Li9U5ezl08jhpdeoF5Jyztm9g7NSnKXE5y3t4Vufs5bbPp/LhumV8du0dJDli47KvnheRsEjA91wOw482/lgDPAos41QCcRR7f6QngEIfj0/yIw4D70mWv99HIBK1QPL1symjnhepzFOPy+lKPCQ31VVQUszl771AkbOk0o7YFhYW8O229fz9h68Dcq5IoORFJCy64rteS1dq/itaBLxUei5382u2A1/4eI6ebh5bkYm9X5G3xKM1kOzjPGAvA48kmQFuJ/FiYPM2HntdyrSo35CmdQOzC/z7a5ZyuOAELg/DUC7L4pnFM3G6AlHiIPyUvIiERR+gIZ5/BV3AmACcZyn2Kh9v5ffnYK9Y8qQr9uofT7Fa+I41CXtljicm9pLpJj6eJ9SygDZ4/t5NoAWRNdQlkeCnXfqQVqee292pwe6LvHvguZhGYC7Di/duJ9H03nOZfSyPfcdyA3K+cFPyIhIWidjLi8uWAxoV/jSw9/7pEIDz7ML3UMwJ7GEkT0zgLuwEpuzrsreOROAW7OXSvozHTtrKngNOfd8tgRv9eI5wuAG71+j0t0sT+/ufGPKIJPLVSkjk46tvp3ZCIo4KCUpZMnNZ517cNcBbQl89iaajdIDIuyRHpA3NnpnYmLkjEpUygD9hF2hbhT3E0xJ7+XOgCs0l4t++QL5qtKQCD2BPTP0Re3Juc2Ag9l5K/nBgF9Ebjl035gB28jYQu9clUt9Us7C/90+xXysXduLSGzshywhfaBLRBrdox6rb/sjTi77jvTVLOV5USNcmmfyy/0iu6toXhxm4/oOx7bvxxIJvPN5vGgbdmzajSZ3ADFOFm4rUicS0LcDfvNxvYCch/xeacKJeIfay77r4N4dHJDRcloueL/yZdQf2UeJhsvC7l9/E1d36hTgy/1Xn+q1hI5GY1hZ7SMfbfJVxoQsn6iVj94opcZHIYhomn197B20appV+bQ9PJZT27jw84uKITlyqSz0vIjHvGPAUdm0Zk8rDSD8FzgtHUCISBIUlxUxbt5wP1i4jv6iALk0yubXPMDo3ifwVcdW5fit5EYkLLux9g5Zh1yTJwN56IFibOIqIVI/2NhKR05hAt9KbiEh005wXERERiSrqefGTZVnsO5aLZVlkpqQGrLCQiESyI8D3wDrsuUIdgGFAWjiDEol7Sl58sCyLF5bM4fH5X7P1yEEAWqU2YvKg87mj/wglMSIxazXwPPZ+UGVTA3cA3wC/4FTBPREJtaBeeY8cOcKECRNITU0lNTWVCRMmcPToUa+PufHGGzEMo9Jt4MCBwQzTI8uyuPWzt7n9i3fYVpq4AOzIPcyvpr/Hjf97PeDbmYtIJDhM1cQF7InPTuBl/NttW0SCIajJy7XXXsuKFSuYPn0606dPZ8WKFUyYMMHn4y644AL27dtXfvviC18bxwXHt9vW89KyuYD7GqVvrVzI55tWhTYoEQmB2diJircPJzNDFIuInC5ow0br1q1j+vTpLFiwgAEDBgDw0ksvMWjQIDZs2EDHjh09PjY5OZmMjPCX3H5hyRwSDNNjtUKHYfL84tlc1OHsEEcWeLvzjvDlptUUlBTTI6M5Q1u2x/CwoZhI7FuD9520XdjDSiISDkFLXubPn09qamp54gIwcOBAUlNTmTdvntfkZdasWTRt2pQGDRowfPhwHnnkEZo2beq2bWFhIYWFheVf5+XlBex7WLV/t8fEBcBpuViVsydg5wuHk8VFTPp8Km+vXIjLsjANA5dl0bFxOu9efhO9MluGO0SRMPCWuFSnjYgEQ9CGjbKzs90mHE2bNiU7O9vj48aOHcvUqVP57rvv+Mc//sHixYsZNWpUpQSlokcffbR8Tk1qaiotWrQI2PdQv5bvDedSkmoF7HyhZlkWV33wUnniApT/uflwDiPe+AdbDh8IZ4giYdIe72+PZmkbEQmHaicvDz74YJUJtafflixZAuB22MGyLK/DEVdddRUXXngh3bp1Y/z48Xz55Zds3LiRzz//3G37KVOmkJubW37btWtXdb8lz7F07Vu+P4Q7pmFwTffo3Stiwe6tfLpxZXnCUpHTsjhRXMTffvgqDJGJhNtwvM93cQEjQxSLiJyu2sNGd9xxB1dffbXXNq1bt2blypXs37+/yn0HDhwgPT3d7/NlZmbSqlUrNm3a5Pb+5ORkkpODs0naz3udw9/nfc2hE8dxnjZ85DBMUmvV5pY+Q4Ny7lB4e+VCEkyTEpf77u8Sl4u3Vi7ghYuu0/wXiTNZwPXAW9if8cp+R8r+fjn2hpciEg7VTl7S0tJIS/NdoGnQoEHk5uayaNEi+vfvD8DChQvJzc1l8ODBfp/v0KFD7Nq1i8zM0G8q1ah2XWZNvJdx7zzN9qOHSDRNwKDY5SQzJZXPr72DpnWjd/+kgyePue11qehkSTEFJcXUTkwKUVQikWII0Bz4FntfqLIideeiISOR8ArahN3OnTtzwQUXcPPNN/Pvf/8bgFtuuYWLLrqo0mTdTp068eijj3LZZZdx7NgxHnzwQS6//HIyMzPZvn07DzzwAGlpaVx22WXBCtX799Ekk813/pnPN61i1vYNWBYMa9We8R3PJsF0hCWmQGmV2rh8gq4nDWvVoVZCYgijEokkrbEL0olIJAlqhd2pU6dy1113MXr0aAAuvvhinnnmmUptNmzYQG5uLgAOh4NVq1bx5ptvcvToUTIzMxk5ciTvvfceKSkpwQzVK4dpcnHHHlzcsUfYYgiGsmExTxyGya19hmnISEREIophxViJ2OpsqS0w+av/8s8F31Y5nmCaNK/fkMU3P0BanXphiExEROJJda7f2pgnzv1j9BU8fv5PKyUoDsPksk49mf+L3ypxERGRiKOeFwGg2Olkyd7tFJSU0KVJJun1vP/sCkqKeWXZXF5YModtRw+SWqsOE84ewF0DRpGV0iA0QYuISMyozvVbyYtU2/GiQs5/60kW7N4KnKqGUbZ8fM6Nv6Zr06zwBSgiIlFHw0YSVL/77n8s2rMNi8plvJyWi9yCk/zkvy9ot20REQkaJS9SLceLCnlp2VycHpITp+Vi46H9zNy+IcSRiYhIvFDyItWy/mA2J4qLvLZxGCYLd28LUUQiIhJvlLxItSSY/vyTsfxsJyIiUn26wki1dGmSRdO63gsGOi2L0e26hCgiERGJN0pepFoSHQ5+Peh8j/c7DJORrTvQI6NFCKMSEZF4EtTtASQ23Tv4fDYezuHlZXPLd6Uu2yOpW9MsHhpxMXdPf48Nh/ZTP7kWV3bpy8Ude5DoiO69oEREJDKozoucEcuyWLB7Ky8um8vmQzk0rlOXa7r148f9u3l07vTypMZhmDgtF92aZvHNhHt8Fr8TEZH4pCJ1Sl7C4rXlP/DzT950e1+CadIvqzU//Pw+bfQoIiG14WA2u/OOkF6vPl2bZOk9KEJV5/qtYSMJCMuyeHTudAwqF64rU+JyMX/3Vhbu2cbA5m1DHZ6IxKEfdm7mnq/eZ/He7eXHujdtxj9G/5TztaggqmnCrgTEjtxDbDqc4zZxKZNgmnyxaXXIYhKR+PX9jk2MfOMJlu7bUen46py9XDD1KT7fuCpMkUkgKHmRgChyOn22MTAocpaEIBoRiWeWZXH7F+/gtFy4TpsZYWFhWRa3fT4Vp8sVpgilppS8SEC0Sm1Eg+TaXtsUu5z0zWoVoohEJF6tyN7F6py9VRKXMhawK+8Is7SNSdRS8iIBkZyQyKS+wzE9TIQzDYP0uvW5pGPP0AYmInFnR+5hv9rt9LNdoMTY+piw0oRdCZg/DL+QuTs388OuLdids7YE0yTJkcC0qyap1ouIBF1anXp+tWvsZ7ua2Hw4hyfmf8PUVQvJLyygTcM0bus7nNv7jaBOYlLQzx+rtFRaAqqgpJgXl37Pc4tnseXIAeomJnNNt35MHnQe7Runhzs8EYkQe/OPsvbAPuokJtEvq3VAP9g4XS5a/2sKu/OOemzToFYd9t37N2olJAbsvKdbuHsb5775BIXOEkoqzK8xDYPeGS2ZeeNk6iXVCtr5o43qvCh5ERGJSLvzjnDHF+/yyYaV5f2zTerUY8qQsdw98NyA1WB5d9Uirp32isf7nx13Dbf3GxGQc7lT4nLS+skHyD6Wi9PNZdZhmNzZfyT/vODKoMUQbapz/dacFxERCYnsY7kMfPkxPt+4qsLAMhw4cYzJX7/P/d9MC9i5runen9cumVi+kKBsPl69pGT+dcFVQU1cAL7YtJo9+UfdJi4ATsvFy8vncqK4KKhxxCrNeZGYV1hSzOsr5vP8ktlsPXKQBrVqc0OPgdzRfyQZ9VLDHZ5I3Hhs7nSyj+XhtNwvUf77vK+5pc8w2jVqEpDz3dhzMFd368dnG1eyO+8IGfVSGd/hbOomJQfk+b1ZuncHiaaDYpfnMhLHigrZcvgA3dObBT2eWKPkRWLa8aJCxrz9L+bt2gIYWFjkFxXw2Nzp/Hvp98y58dd0bpIZ7jBFYp7T5eKV5T94TFwATMPktRU/8OdRlwbsvLUSEvlplz4Bez5/JTkSPC7VrtxOixjOhIaNJKb933cfs2D3Viyo1E3ttCyOnDzBT/77gpYvioRAflEBx4oKfbbbmXskBNEE37j23bwmagCtGzSmfeOmIYootih5kZh1vKiQl5Z973XMef3BbGbv2BjiyETiT93EZJ+9DIZhT96NBb0yWzKqdUcchufL7JQhF2B6uV88009NYta6g/s47mMynMMwWbB7W4giEolfiQ4HV3frR4Lp+bJT4nJx/dkDQhhVcP33ilvoldECoDyJKfv+f3vOGG7uPTRssUU7zXmRmOXtE88pFo4ALc0UEe8eGDKWaeuWc7K4uMqQimkYXN65N70yW4YpusBrXKceC266ny83r+Y/qxdztOAk7Rs15abeQ+jaNCvc4UU1JS8Ss7o1bUaTOvU4cOKYxzZOy2J0uy4hjEokfnVMy2DWxHu5dtorbDy0HwN7nyGHYfKLXufw1Nirwh1iwDlMk4s6nM1FHc4OdygxRcmLxKxEh4PJg87ngW8/wt2slwTT5JwW7ehR2q0rIsHXJ6sV63/5EN/v3MSq/XuonZjEuPbdVLZAqkXJi8S03wwezYZD2by+Yj4JpkmJy4VpGLgsi05pGfz3ilvCHaJI3DEMg2GtOjCsVYdwhyJRSsmLxDSHafLqxRP5Wc/BvLxsLpsO59C4dj2u696fn3TuRXIQ9zUREZHgUPIiMU+f8kREYouWSouIiEhUUfIiIiIiUUXJi4iIiEQVJS8iIiISVZS8iIiISFRR8iJSTQUlxew4eogjJ4+HOxQRkbikpdIifso5nsdDsz7jtRXzOFlSDMB5bTrxxxHjGdLyrDBHJyISP5S8iPhh/7E8Brz8KLvzjlbaUG7m9g3MfH0jH101ifEde4QxwuiVX1jA80tm8+8lc9iVd4RGtetwQ49B3D3wXLJSGoQ7PBGJQIZlWe62fYlaeXl5pKamkpubS/369cMdjsSIn338Bm+vXECJy1XlPgNIrVWbfff+nVqq2FstR04eZ9jrj7P2wD5cFd6KHIZJw9p1mHPjr+ncJDOMEca+E8VFfLlpNYdPHqd1g8aMatMJh6kZBRJ61bl+q+dFxIe8wpO8s2qh28QF7F1xjxacZNq65VzbvX9og4ty93z1PusOZFdKXACclosjJ09w1Qcv8eOk32MYRpgijF2WZfHPBd/w4KxPyS8qLD/evH5DXrzoesa27xbG6ES8U3ot4sOOo4cpcjq9tkk0Haw7sC9EEcWGQyeO8c6qRZWG4SpyWi5W5exh/u6tIY4sPvx93tfc+/UHlRIXgD15Rxn/7rPM3LYhTJGJ+KbkRcSHeknJPtu4LJdf7eSU1Tl7KXZ5TwpNw2Dxnu2hCSiO5Bac5I+zPnF7n1X6329mfBDiqET8p+RFxIfWDRrTvWkzDDwPXTgti5907hXCqKJfksPhs41lWSQ5NLodaB+tX05hSYnH+12WxdJ9O9l4aH8IoxLxn5IXER8Mw+ChEeOxcD+33TQMru7aj/aN00McWXTrndmSBrXq+Gw35qwuIYgmvuQcz8c0fL/95xzPD0E0ItWn5EXED5d17sW/L7qOJIcDA4NE00FC6YqMyzr15NVLbghzhNEnOSGRewed57E/y2GYXNapF20bNglpXPGgef2GHucaVW7XIPjBiJwB9ceK+OmWPsP4aZc+vL1yIZsP55CaXJsru/ale3qzcIcWtaYMGcvWIwd5bcU8EkyTEpcLh2HitFwMbN6G1y6dGO4QY9KlnXpSLymZY6dN1i3jMAzOaXkWrRukhTgyEf+ozotIgLksFwaGlvf6ybIsFu3ZzivL57L1yEGa1k3h+rMHMKZdV9UbCaJXls3lpk/fqnLcNAySHA6+/9lv6JvVOvSBSdxSnReRECtxOXl52VyeXjSTdQf2kehwcEnHHvxm8Bj6NWsd7vAimmEYDGjehgHN24Q7lLjyi95DqJ2YxP3fTGNX3pHy470yWvDchdcqcZGIpp4XkRoqcTm5/L0X+HTjSoDyab0JpollwXs/vZnLu/QOX4AiXjhdLhbu2cahE8do0zCNbk01DCrhoZ4XkRB6YckcPt24sspapBKXCwO4btorjGjdgcZ16oUjPBGvHKbJ4BbtzvjxlmVx8MQxXJZF07opGi6VkFDyIlIDlmXxr4Xfer4fKHY5eX3FfO4dfH7oAhMJMsuyeOPH+fzth69YdzAbsGsi3T3gXO7oP1LzlSSo9K9LpAYKSorZfPiAhwowNgNYlr0zVCGJhMR9Mz7kZx+/wfqDpwrZbT96iHu++i/XT3sFlx9LsUXOlJIXkRpIMB1e6u7aDMMgWVViJYbM37WFx+fPAKhSvNEC/rNmCdPWLQ9DZBIvlLyI1ECiw8H5bbvg8FKttMTl4qIO3UMYlUhwvbB0TnmRRncchsGzi2eFLiCJO0FNXh555BEGDx5MnTp1aNCggV+PsSyLBx98kKysLGrXrs2IESNYs2ZNMMMUqZHfDhnjsYs8wTBp17AJ4zv0CHFUIsGzcv8eSlyeh4WclsXqnL0hjEjiTVCTl6KiIq644gpuu+02vx/zt7/9jSeeeIJnnnmGxYsXk5GRwfnnn09+vvbYkMg0qk0nXhw/AYdh4ihdaVH2Z4vURsyYcDeJfmxCGGgxVgVBIkhKUrLP4VLtsi7BFNSB+IceegiA119/3a/2lmXx5JNP8rvf/Y6f/OQnALzxxhukp6fzzjvvcOuttwYrVJEauan3EMa068LLy+ayMmcPtRISuaRjD37SuVdId0Xel5/LE/Nn8NqKeRw6eZymdVO4ufcQ7h54Hmlaqh31jpw8zmcbV5FbeJL2jZpyXtvOYVnVc0WXPszdudnj/Q7D5KqufUMYkcSbiJpFuG3bNrKzsxk9enT5seTkZIYPH868efPcJi+FhYUUFp7anyMvLy8ksYqcrkVqIx4aeXHYzr/l8AEGv/pXDp04Xr7pXs7xfB6b+xVvrVzIvJ/fR7P6DcMWn5w5l+XiDzM/4fF5Myh0lmBgT4xtXr8hr10ykfPadg5pPBN7DuLRudPJOZ5fZYNHh2FQJzGJX/YbEdKYJL5E1ITd7Gy7VkB6enql4+np6eX3ne7RRx8lNTW1/NaiRYugxykSiSZ89GqlxKWM03KxN/8okz6bGqbIpKZ+O2Maj3z/JYXOEuBUFee9+UcZO/Up5u3aEtJ46ifXZubEybRMbQRAommSaNpDo43r1OObG+6mRel9IsFQ7eTlwQcfxDAMr7clS5bUKKjTKzRaluWxauOUKVPIzc0tv+3atatG5xaJRqv272H+7q1VEpcyJS4Xn29axY6jh0IcmdTU3vyjPLHgG7f3uSwLy7L4v+8+DnFU0DEtg013/omPr76dSX2Hc0ufobzzk1+w657H6N9M+1RJcFV72OiOO+7g6quv9tqmdevWZxRMRkYGYPfAZGZmlh/Pycmp0htTJjk5meRkTQyT+LZsn+8ieBawInsXrRo0Dn5AEjDvrfb+YdBpWczcvoF9+blkpqSGKCqbwzS5uGMPLu6o1XQSWtVOXtLS0khLSwtGLLRp04aMjAxmzJhBr169AHvF0uzZs/nrX/8alHOKxIIkP1czJSdE1DQ38cOBE/k4DBOX5fTa7uCJYyFPXkTCJahzXnbu3MmKFSvYuXMnTqeTFStWsGLFCo4dO1beplOnTnz00UeAPVx0991385e//IWPPvqI1atXc+ONN1KnTh2uvfbaYIYqEtXObdvJa9EwgLqJSQxpeVaIIpJAaZnaiBKX98TFNAyylLhIHAnqx7A//OEPvPHGG+Vfl/WmzJw5kxEjRgCwYcMGcnNzy9vcd999nDx5kttvv50jR44wYMAAvv76a1JSUoIZqkhUa1q3Pr/odQ4vLZuLy019FwODO/uPol5SrTBEJzVxdbd+3D39v+WTdU+XYNhDN9q1XOKJYcVYJau8vDxSU1PJzc2lfv364Q5HJGQKSoq58v0X+XTjShJMkxKXq/zP67r35/VLbyTBDH2xPKm5pxZ+x6+mv1fluMMwqZuUxKKbptAxLSMMkYkETnWu3xoAF4kRtRIS+fjq25m7czNv/riA7OO5NE9pyM96Ddbqjyh314BRpCbX5vczP2ZX3pHy48NateeZcVcrcZG4o54XEZEo4bJcLNm7g9yCk7Rr1IS2DZuEOySRgFHPi4hIDDINU71oIkRYhV0RERERX5S8iIiISFTRsJGIcLTgBOsPZpPsSKB7ejOtShKRiKbkRSSOHT55nN/M+JC3Vy6gyGkXQsusl8r9Qy7gzv4jPe4pJiISTkpeROJUXuFJhrz6NzYeyqm0oeO+Y7n8avp77Mw9zOOjfxrGCEVE3NOcF5E49a8F37Hx0H6PO1H/Y/4M1uTsDXFUIiK+KXkRiVMvLJ2N00uZpwTT5JXlc0MYkYiIfzRsJBKHnC4Xe/NzvbYpcbnYeuRgUM5vWRbTN6/h+SWzWZWzh/pJtbiya19u6TOUJnW1j5mIeKfkRSQOOUyTeknJHCsq9NgmwTRpXDvwm/25LBc3f/oWry6fh8Mwy4etVh/Yyz8XfMPMiffSPb1ZwM8rIrFDw0Yicer67gNIMD2/BZS4XFzbvV/Az/v84tm8unweQKX5Ni7L4mjBSS5852lKXM6An1dEYoeSF5E49evBo6mdkITDqPo24DBMhrfqwKg2nQJ6Tsuy+Mf8GXhagO20XOzKO8InG34M6HlFJLYoeRGJU+0aNWHWjffSqkEjwE5YzNK6Lhd3PJtPrrk94HVeso/lse3oIbztBptomszZsSmg5xWR2KI5LyJxrHdmSzbd+Se+27aBZft2kuxIYFz7brRvnB6U8/mXC6kwnoh4p+RFJM6Zhsl5bTtzXtvOQT9Xet36tGvYhK1HDnjsfSl2ORneqkPQYxGR6KVhIxEJGcMwuHfQ+R4TF4dh0jK1EeM7nh3SuEQkuih5EZGQurXvUG7qPQSg0mon0zBoWLsOn197hzaGFBGvNGwkIiFlGiYvXnQ9V3Tpw3OLZ7E6Zy8pSclc1a0fN/UeQlqdwNeWEZHYouRFRELOMAxGt+vC6HZdwh2KiEQhDRuJiIhIVFHyIiIiIlFFyYuIiIhEFSUvIiIiElWUvIiIiEhUUfIiIiIiUUXJi4iIiEQVJS8iIiISVZS8iIiISFRR8iIiIiJRRcmLiIiIRBUlLyIiIhJVtDGjiMQ1l+Vi9vZN7Mw9TFqdepzfrjNJDr01ikQy/YaKSNz6YtMqbvv8HXbmHi4/1qh2XR479zJu7jM0jJGJiDdKXkQkLn29ZS3j330Wy6p8/PDJ49zy2ds4LReT+g4PT3Ai4pXmvIhI3LEsi3u++q/9dyy3be7/Zhoni4tCGZaI+EnJi4jEnVU5e1h7YB+u07tdKsgtLODzTatCGJWI+EvJi4jEnexjeT7bGBh+tROR0FPyIiJxJysl1WcbC4tmKQ2CH4yIVJuSFxGJO92aNqNnRgtMw/DYpkGtOoxr3y2EUYmIv5S8iEhcenLMlZiGgYn7BObJMVeSnJAY4qhExB9KXkQkLg1v3YGvr7+bjmnplY43S2nA1J/8gok9B4UpMhHxxbAsL9Pto1BeXh6pqank5uZSv379cIcjIhHOsiyW7N3BztzDNKlbj3NanIXD1Oc6kVCrzvVbRepEJK4ZhkG/Zq3p16x1uEMRET/p44WIiIhEFSUvIiIiElWUvIiIiEhUUfIiIiIiUUXJi4iIiEQVJS8iIiISVZS8iIiISFRR8iIiIiJRRcmLiIiIRJWYq7BbtttBXl5emCMRERERf5Vdt/3ZtSjmkpf8/HwAWrRoEeZIREREpLry8/NJTU312ibmNmZ0uVzs3buXlJQUDMP9VveRIi8vjxYtWrBr1y5tIhlmei0ii16PyKHXIrLE8uthWRb5+flkZWVh+tgcNeZ6XkzTpHnz5uEOo1rq168fc/8Io5Vei8ii1yNy6LWILLH6evjqcSmjCbsiIiISVZS8iIiISFRR8hJGycnJ/PGPfyQ5OTncocQ9vRaRRa9H5NBrEVn0ethibsKuiIiIxDb1vIiIiEhUUfIiIiIiUUXJi4iIiEQVJS8iIiISVZS8hNAjjzzC4MGDqVOnDg0aNPDrMZZl8eCDD5KVlUXt2rUZMWIEa9asCW6gceLIkSNMmDCB1NRUUlNTmTBhAkePHvX6mBtvvBHDMCrdBg4cGJqAY8xzzz1HmzZtqFWrFn369OH777/32n727Nn06dOHWrVq0bZtW1544YUQRRr7qvNazJo1q8rvgGEYrF+/PoQRx6Y5c+Ywfvx4srKyMAyD//3vfz4fE6+/F0peQqioqIgrrriC2267ze/H/O1vf+OJJ57gmWeeYfHixWRkZHD++eeX7+EkZ+7aa69lxYoVTJ8+nenTp7NixQomTJjg83EXXHAB+/btK7998cUXIYg2trz33nvcfffd/O53v2P58uUMHTqUsWPHsnPnTrftt23bxrhx4xg6dCjLly/ngQce4K677uLDDz8MceSxp7qvRZkNGzZU+j1o3759iCKOXcePH6dHjx4888wzfrWP698LS0Lutddes1JTU322c7lcVkZGhvXYY4+VHysoKLBSU1OtF154IYgRxr61a9dagLVgwYLyY/Pnz7cAa/369R4fN3HiROuSSy4JQYSxrX///takSZMqHevUqZN1//33u21/3333WZ06dap07NZbb7UGDhwYtBjjRXVfi5kzZ1qAdeTIkRBEF78A66OPPvLaJp5/L9TzEsG2bdtGdnY2o0ePLj+WnJzM8OHDmTdvXhgji37z588nNTWVAQMGlB8bOHAgqampPn+2s2bNomnTpnTo0IGbb76ZnJycYIcbU4qKili6dGmlf9cAo0eP9viznz9/fpX2Y8aMYcmSJRQXFwct1lh3Jq9FmV69epGZmcm5557LzJkzgxmmeBDPvxdKXiJYdnY2AOnp6ZWOp6enl98nZyY7O5umTZtWOd60aVOvP9uxY8cydepUvvvuO/7xj3+wePFiRo0aRWFhYTDDjSkHDx7E6XRW6991dna22/YlJSUcPHgwaLHGujN5LTIzM3nxxRf58MMPmTZtGh07duTcc89lzpw5oQhZKojn34uY21U61B588EEeeughr20WL15M3759z/gchmFU+tqyrCrHxObv6wFVf67g+2d71VVXlf+9W7du9O3bl1atWvH555/zk5/85Ayjjk/V/Xftrr2741J91XktOnbsSMeOHcu/HjRoELt27eLxxx9n2LBhQY1TqorX3wslLzV0xx13cPXVV3tt07p16zN67oyMDMDOrjMzM8uP5+TkVMm2xebv67Fy5Ur2799f5b4DBw5U62ebmZlJq1at2LRpU7VjjVdpaWk4HI4qn+y9/bvOyMhw2z4hIYHGjRsHLdZYdyavhTsDBw7k7bffDnR44kM8/14oeamhtLQ00tLSgvLcbdq0ISMjgxkzZtCrVy/AHqOePXs2f/3rX4Nyzmjn7+sxaNAgcnNzWbRoEf379wdg4cKF5ObmMnjwYL/Pd+jQIXbt2lUpuRTvkpKS6NOnDzNmzOCyyy4rPz5jxgwuueQSt48ZNGgQn376aaVjX3/9NX379iUxMTGo8cayM3kt3Fm+fLl+B8Igrn8vwjlbON7s2LHDWr58ufXQQw9Z9erVs5YvX24tX77cys/PL2/TsWNHa9q0aeVfP/bYY1Zqaqo1bdo0a9WqVdY111xjZWZmWnl5eeH4FmLKBRdcYJ199tnW/Pnzrfnz51vdu3e3LrrookptKr4e+fn51r333mvNmzfP2rZtmzVz5kxr0KBBVrNmzfR6VNN//vMfKzEx0XrllVestWvXWnfffbdVt25da/v27ZZlWdb9999vTZgwobz91q1brTp16lj33HOPtXbtWuuVV16xEhMTrQ8++CBc30LMqO5r8c9//tP66KOPrI0bN1qrV6+27r//fguwPvzww3B9CzEjPz+//LoAWE888YS1fPlya8eOHZZl6feiIiUvITRx4kQLqHKbOXNmeRvAeu2118q/drlc1h//+EcrIyPDSk5OtoYNG2atWrUq9MHHoEOHDlnXXXedlZKSYqWkpFjXXXddleWfFV+PEydOWKNHj7aaNGliJSYmWi1btrQmTpxo7dy5M/TBx4Bnn33WatWqlZWUlGT17t3bmj17dvl9EydOtIYPH16p/axZs6xevXpZSUlJVuvWra3nn38+xBHHruq8Fn/961+tdu3aWbVq1bIaNmxoDRkyxPr888/DEHXsKVuGfvpt4sSJlmXp96Iiw7JKZ/eIiIiIRAEtlRYREZGoouRFREREooqSFxEREYkqSl5EREQkqih5ERERkaii5EVERESiipIXERERiSpKXkRERCSqKHkRERGRqKLkRURERKKKkhcRERGJKkpeREREJKr8Pzd+3852fI9uAAAAAElFTkSuQmCC",
      "text/plain": [
       "<Figure size 640x480 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "X, y = make_circles(n_samples=100, noise=0.1, factor=0.3, random_state=0)\n",
    "y = y.reshape((y.shape[0], 1))\n",
    "\n",
    "print('Dimensions de X:', X.shape)\n",
    "print('Dimensions de y:', y.shape)\n",
    "\n",
    "plt.scatter(X[:, 0], X[:, 1], c=y, cmap='summer')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  },
  "vscode": {
   "interpreter": {
    "hash": "f70f37d00246f496a7db06db0e7e3ad0818f33e8a9958fe0e709733752c74ccd"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
